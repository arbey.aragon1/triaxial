using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public GameObject btmMsgPlay;
    public GameObject btmMsgPause;
    public GameObject text;
    bool valid = true;
    public AudioClip clip;
    public AudioSource player;
    public SliderController sliderController;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<AudioSource>();
        player.clip = clip;
    }

    void Update()
    {
        int minutes = (int) player.time / 60;
        int seconds = (int) player.time % 60;
        if(!CheckNullSong()){
            if(player.isPlaying){
                sliderController.SetValue((float)player.time/(float)player.clip.length);
                text.GetComponent<TMPro.TextMeshProUGUI> ().text = minutes.ToString("00") + ":" + seconds.ToString("00");
            }
        }
    }

    public void BtnPressed()
    {
        if(!CheckNullSong()){
            if(player.isPlaying){
                player.Pause();
                btmMsgPlay.SetActive(true);
                btmMsgPause.SetActive(false);
            }else{
                player.Play();
                btmMsgPlay.SetActive(false);
                btmMsgPause.SetActive(true);
            }
            
        }
    }

    bool CheckNullSong(){
        if(player.clip == null){
            return true;
        }
        return false;
    }

    
    public void OnChangeSlider()
    {
         if(!player.isPlaying){
            //player.Pause();
            player.time = (float)sliderController.value*(float)player.clip.length;
        }
    }
}
