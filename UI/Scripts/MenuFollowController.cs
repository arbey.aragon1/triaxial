using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFollowController : MonoBehaviour {
    private GameObject _camera;
    private const float _distance = 1.5f;
    private bool _follow = false;

    void Start () {
        _camera = GameObject.Find ("Main Camera");
    }

    void Update () {
        HeadLock (gameObject, 5.0f);
    }

    public void HeadLock (GameObject obj, float speed) {
        if(_camera == null){
            _camera = GameObject.Find ("Main Camera");
        }
        speed = Time.deltaTime * speed;
        Quaternion rotTo = Quaternion.LookRotation ( - _camera.transform.position + obj.transform.position);
        if(_follow){
            obj.transform.rotation = Quaternion.Slerp (obj.transform.rotation, rotTo, speed);
            obj.transform.position = _camera.transform.position + _camera.transform.forward * 1.0f;
        }
    }

    public void SetFollow(bool val){
        _follow = val;
    }

    public void UpdateFollow(){
        _follow = !_follow;
    }
}