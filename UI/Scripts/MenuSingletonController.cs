using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSingletonController : MonoBehaviour
{
    public static MenuSingletonController instance;
    void Awake()
    {
        if(MenuSingletonController.instance == null) {
            MenuSingletonController.instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    
}
