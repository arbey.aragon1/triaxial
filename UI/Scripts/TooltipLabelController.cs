using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class TooltipLabelController : MonoBehaviour
{
    public string textLabel = "--";
    private GameObject tooltipGo;
    private GameObject centerGo;
    private LineRenderer lineRender;
    private TooltipController tooltipController;
    private Vector3 posCenter;
    private Vector3 posLabel;
    private Vector3 posLabelBound;
    private Vector3 dir;
    private BoxCollider boxCollider;
    private int indexBound = 0;
    public TooltipCenterController tooltipCenterController;

    private void Start()
    {
        this.lineRender = gameObject.AddComponent(typeof(LineRenderer)) as LineRenderer;
        this.boxCollider = GetComponent<BoxCollider>();
        
        this.lineRender.alignment = LineAlignment.View;
        this.lineRender.endColor = Color.white;
        this.lineRender.startColor = Color.white;
        this.lineRender.numCapVertices = 3;
        this.lineRender.numCornerVertices = 3;
        this.lineRender.useWorldSpace = true;
        this.lineRender.startWidth = 0.003f;
        this.lineRender.material = SettingsController.GetLineTooltipsMat();
        

        this.posCenter = this.boxCollider.bounds.center;

        this.dir = tooltipCenterController.GetMeanDirection(this.posCenter);

        this.posLabel = this.posCenter + dir * 0.2f;

        this.lineRender.SetPosition(0, this.posCenter);
        this.lineRender.SetPosition(1, this.posLabel);
        
        tooltipGo = Instantiate(Resources.Load<GameObject>("Tooltip/msg-tooltip"), this.posLabel, Quaternion.identity);
        centerGo = Instantiate(Resources.Load<GameObject>("Tooltip/center"), this.posCenter, Quaternion.identity);

        tooltipGo.transform.SetParent(transform);
        centerGo.transform.SetParent(transform);

        tooltipGo.SetActive(true);
        this.tooltipController = tooltipGo.GetComponent<TooltipController>();
        this.tooltipController.SetText(textLabel);

        UpdateReferences();

    }

    private void Update()
    {
        this.posCenter = this.centerGo.transform.position;
        this.lineRender.SetPosition(0, this.posCenter);

        this.posLabelBound = this.tooltipController.bounds[this.indexBound].transform.position;
        
        this.lineRender.SetPosition(1, this.posLabelBound);
        tooltipGo.transform.position = this.centerGo.transform.position + dir * 0.2f;
    }

    public void UpdateReferences()
    {
        int count = 0;
        float distanceMin;
        float distance;
        distanceMin = Vector3.Distance(tooltipGo.transform.position, this.centerGo.transform.position);
        foreach (GameObject item in this.tooltipController.bounds)
        {
            distance = Vector3.Distance(item.transform.position, this.centerGo.transform.position);
            if(distanceMin > distance)
            {
                this.indexBound = count;
            }
            count++;
        }
        this.dir = tooltipCenterController.GetMeanDirection(this.centerGo.transform.position);
    }
}
