using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControlPoints
{
    public enum TypeLine
    {
        line,
        hermite
    }

    public TypeLine type;

    public List<GameObject> controlPoints;
}

public class MultiLineGenerator : MonoBehaviour
{
    private LineRenderer lineRender;

    public ControlPoints[] controlPointsList;

    public int numberOfPoints = 3;
    public float duration = 10.0f;
    public int repetitions = 5;
    
    private List<Vector3> line;

    private int loopLine = 1;
    private bool reversed = false;
    private float animationDuration = 5f;

    void Start()
    {
        this.lineRender =
            gameObject.AddComponent(typeof (LineRenderer)) as LineRenderer;
        this.lineRender.alignment = LineAlignment.View;
        this.lineRender.endColor = Color.white;
        this.lineRender.startColor = Color.white;
        this.lineRender.numCapVertices = 3;
        this.lineRender.numCornerVertices = 3;
        this.lineRender.useWorldSpace = false;
        this.lineRender.startWidth = 0.008f;
        this.lineRender.material = SettingsController.GetLineWaterMat();

        line = new List<Vector3>();
        Vector3[] l = new Vector3[0];
        foreach (ControlPoints cp in controlPointsList)
        {
            if (cp.type == ControlPoints.TypeLine.line)
            {
                l = GenerateLine(cp.controlPoints);
            }
            else if (cp.type == ControlPoints.TypeLine.hermite)
            {
                l = GenerateHermiteLine(cp.controlPoints);
            }
            for (int j = 0; j < l.Length; j++)
            {
                line.Add(l[j]);
            }
        }
    }

    public void Empty()
    {
        StopCoroutine("AnimateLine");
        if(reversed){
            line.Reverse();
        }
        this.lineRender.enabled = false;
    }

    public void Fill()
    {
        StopCoroutine("AnimateLine");
        if(reversed){
            line.Reverse();
        }
        this.lineRender.enabled = true;
        int n = line.Count;
        this.lineRender.positionCount = n;
        for (int i = 0; i < n; i++)
        {
            this
                .lineRender
                .SetPosition(i, line[i] - gameObject.transform.position);
        }

    }

    public void RunLine(int loopLine, float duration)
    {
        this.lineRender.enabled = true;
        StopCoroutine("AnimateLine");
        this.loopLine = loopLine;
        this.animationDuration = duration;
        if(reversed){
            line.Reverse();
        }
        StartCoroutine("AnimateLine");
    }

    public void RunReverseLine(int loopLine, float duration)
    {
        this.lineRender.enabled = true;
        this.loopLine = loopLine;
        this.animationDuration = duration/loopLine;
        StopCoroutine("AnimateLine");
        line.Reverse();
        reversed = true;
        StartCoroutine("AnimateLine");
    }

    private Vector3[] GenerateLine(List<GameObject> controlPoints)
    {
        Vector3[] linePoints = new Vector3[controlPoints.Count];
        for (int j = 0; j < controlPoints.Count; j++)
        {
            linePoints[j] = controlPoints[j].transform.position;
        }
        return linePoints;
    }

    private Vector3[] GenerateHermiteLine(List<GameObject> controlPoints)
    {
        if (controlPoints == null || controlPoints.Count < 2)
        {
            return null;
        }

        if (numberOfPoints < 2)
        {
            numberOfPoints = 2;
        }

        Vector3[] linePoints =
            new Vector3[numberOfPoints * (controlPoints.Count - 1)];

        Vector3
            p0,
            p1,
            m0,
            m1;

        for (int j = 0; j < controlPoints.Count - 1; j++)
        {
            if (
                controlPoints[j] == null ||
                controlPoints[j + 1] == null ||
                (j > 0 && controlPoints[j - 1] == null) ||
                (j < controlPoints.Count - 2 && controlPoints[j + 2] == null)
            )
            {
                return null;
            }

            p0 = controlPoints[j].transform.position;
            p1 = controlPoints[j + 1].transform.position;

            if (j > 0)
            {
                m0 =
                    0.5f *
                    (
                    controlPoints[j + 1].transform.position -
                    controlPoints[j - 1].transform.position
                    );
            }
            else
            {
                m0 =
                    controlPoints[j + 1].transform.position -
                    controlPoints[j].transform.position;
            }
            if (j < controlPoints.Count - 2)
            {
                m1 =
                    0.5f *
                    (
                    controlPoints[j + 2].transform.position -
                    controlPoints[j].transform.position
                    );
            }
            else
            {
                m1 =
                    controlPoints[j + 1].transform.position -
                    controlPoints[j].transform.position;
            }

            Vector3 position;
            float t;
            float pointStep = 1.0f / numberOfPoints;

            if (j == controlPoints.Count - 2)
            {
                pointStep = 1.0f / (numberOfPoints - 1.0f);
            }
            for (int i = 0; i < numberOfPoints; i++)
            {
                t = i * pointStep;
                position =
                    (2.0f * t * t * t - 3.0f * t * t + 1.0f) * p0 +
                    (t * t * t - 2.0f * t * t + t) * m0 +
                    (-2.0f * t * t * t + 3.0f * t * t) * p1 +
                    (t * t * t - t * t) * m1;

                linePoints[i + j * numberOfPoints] = position;
            }
        }
        return linePoints;
    }

    private IEnumerator AnimateLine()
    {
        List<Vector3> linePoints = line;
        int n = line.Count;
        for (int x = 0; x < loopLine; x++)
        {
            this.lineRender.positionCount = n;
            float segmentDuration = animationDuration / n;

            for (int i = 0; i < n - 1; i++)
            {
                float startTime = Time.time;

                Vector3 startPosition = linePoints[i];
                Vector3 endPosition = linePoints[i + 1];

                float dis = Vector3.Distance(linePoints[i + 1], linePoints[i]);

                Vector3 pos = startPosition;
                if (i == 0)
                {
                    this
                        .lineRender
                        .SetPosition(0, pos - gameObject.transform.position);
                }
                float t = 0.0f;

                while (t < 1.0f)
                {
                    t = (Time.time - startTime) / segmentDuration;
                    pos = Vector3.Lerp(startPosition, endPosition, t);

                    for (int j = i + 1; j < n; j++)
                    {
                        this
                            .lineRender
                            .SetPosition(j,
                            pos - gameObject.transform.position);
                    }

                    yield return null;
                }
            }
        }
        if(reversed){
            line.Reverse();
        }
    }
}
