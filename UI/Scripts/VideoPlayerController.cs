using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerController : MonoBehaviour
{
    public VideoPlayer video;
    public GameObject text;
    public GameObject btmMsgPlay;
    public GameObject btmMsgPause;
    bool valid = true;
    //public AudioClip clip;
    //public AudioSource player;
    public SliderController sliderController;
    
    void Start()
    {
        //player = GetComponent<AudioSource>();
        //player.clip = clip;
    }

    void Update()
    {
        int minutes = (int) video.time / 60;
        int seconds = (int) video.time % 60;
        
        if(video.isPlaying){
            sliderController.SetValue((float)video.frame/(float)video.frameCount);
            text.GetComponent<TMPro.TextMeshProUGUI> ().text = minutes.ToString("00") + ":" + seconds.ToString("00");
        }
    }

    public void BtnPressed()
    {
        /*if(!CheckNullSong()){
            if(player.isPlaying){
                //player.Pause();
                btmMsgPlay.SetActive(true);
                btmMsgPause.SetActive(false);
            }else{
                //player.Play();
                btmMsgPlay.SetActive(false);
                btmMsgPause.SetActive(true);
            }
            
        }/***/
        if(video.isPlaying){
            video.Pause();
            btmMsgPlay.SetActive(true);
            btmMsgPause.SetActive(false);
        }else{
            video.Play();
            btmMsgPlay.SetActive(false);
            btmMsgPause.SetActive(true);
        }

    }

    /*bool CheckNullSong(){
        if(player.clip == null){
            return true;
        }
        return false;
    }/**/

    
    public void OnChangeSlider()
    {
        if(!video.isPlaying){
            //player.Pause();
            float frame = (float)sliderController.value*(float)video.frameCount;
            video.frame = (long) frame;
        }
    }
}
