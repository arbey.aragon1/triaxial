using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class SimpleTooltip : MonoBehaviour
{
    public Transform center;
    public string textLabel = "--";
    private LineRenderer lineRender;
    private GameObject tooltipGo;
    private BoxCollider boxCollider;

    public Transform originOptional = null;
    [SerializeField]
    private float lineDimension = 0.2f;
    
    private TooltipController tooltipController;

    void Start()
    {
        this.boxCollider = GetComponent<BoxCollider>();
        var posCenter = this.boxCollider.bounds.center;
        if(originOptional != null){
            posCenter = originOptional.position;
        }
        var dir = (posCenter - center.position).normalized;

        var posLabel = posCenter + dir * lineDimension;

        this.tooltipGo = Instantiate(Resources.Load<GameObject>("Tooltip/msg-tooltip"), posLabel, Quaternion.identity);
        GameObject centerGo = Instantiate(Resources.Load<GameObject>("Tooltip/center"), posCenter, Quaternion.identity);
        centerGo.transform.SetParent(transform);

        this.lineRender = centerGo.gameObject.AddComponent(typeof(LineRenderer)) as LineRenderer;
        
        this.lineRender.alignment = LineAlignment.View;
        this.lineRender.endColor = Color.white;
        this.lineRender.startColor = Color.white;
        this.lineRender.numCapVertices = 3;
        this.lineRender.numCornerVertices = 3;
        this.lineRender.useWorldSpace = false;
        this.lineRender.startWidth = 0.003f;
        this.lineRender.material = SettingsController.GetLineTooltipsMat();
        
        this.lineRender.SetPosition(0, posCenter - centerGo.transform.position);
        this.lineRender.SetPosition(1, posLabel - centerGo.transform.position);

        this.tooltipGo.transform.SetParent(transform);
        
        this.tooltipGo.SetActive(true);
        this.tooltipController = this.tooltipGo.GetComponent<TooltipController>();
        this.tooltipController.SetText(textLabel);

    }

}
