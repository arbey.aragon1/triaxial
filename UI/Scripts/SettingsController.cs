using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour
{
    [SerializeField]
    public Material lineTooltips;
    [SerializeField]
    public Material lineWater;

    public static SettingsController instance;
    void Awake()
    {
        if(SettingsController.instance == null) {
            SettingsController.instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    public static Material GetLineTooltipsMat(){
        return instance.lineTooltips;
    }

    public static Material GetLineWaterMat(){
        return instance.lineWater;
    }
}
