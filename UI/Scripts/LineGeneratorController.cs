using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineGeneratorController : MonoBehaviour
{
    private LineRenderer lineRender;

    private Vector3[] linePoints;

    public List<GameObject> controlPoints = new List<GameObject>();

    public int numberOfPoints = 20;

    [SerializeField]
    private float animationDuration = 5f;

    void Start()
    {
        this.lineRender =
            gameObject.AddComponent(typeof (LineRenderer)) as LineRenderer;

        this.lineRender.alignment = LineAlignment.View;
        this.lineRender.endColor = Color.white;
        this.lineRender.startColor = Color.white;
        this.lineRender.numCapVertices = 3;
        this.lineRender.numCornerVertices = 3;
        this.lineRender.useWorldSpace = false;
        this.lineRender.startWidth = 0.008f;
        this.lineRender.material = SettingsController.GetLineWaterMat();

        GenerateLine();
        StartCoroutine(AnimateLine());

        //this.lineRender.material = SettingsController.GetLineTooltipsMat();

        //this.lineRender.SetPosition(0, posCenter - centerGo.transform.position);
        //this.lineRender.SetPosition(1, posLabel - centerGo.transform.position);
    }

    private void GenerateLine()
    {
        this.lineRender.positionCount = (controlPoints.Count);
        this.linePoints = new Vector3[controlPoints.Count];
        for (int j = 0; j < controlPoints.Count; j++)
        {
            linePoints[j] = controlPoints[j].transform.position;
        }
    }

    private void GenerateHermiteLine()
    {
        if (
            null == this.lineRender ||
            controlPoints == null ||
            controlPoints.Count < 2
        )
        {
            return;
        }

        if (numberOfPoints < 2)
        {
            numberOfPoints = 2;
        }

        this.linePoints =
            new Vector3[numberOfPoints * (controlPoints.Count - 1)];
        this.lineRender.positionCount =
            numberOfPoints * (controlPoints.Count - 1);
        Vector3
            p0,
            p1,
            m0,
            m1;

        for (int j = 0; j < controlPoints.Count - 1; j++)
        {
            if (
                controlPoints[j] == null ||
                controlPoints[j + 1] == null ||
                (j > 0 && controlPoints[j - 1] == null) ||
                (j < controlPoints.Count - 2 && controlPoints[j + 2] == null)
            )
            {
                return;
            }

            p0 = controlPoints[j].transform.position;
            p1 = controlPoints[j + 1].transform.position;

            if (j > 0)
            {
                m0 =
                    0.5f *
                    (
                    controlPoints[j + 1].transform.position -
                    controlPoints[j - 1].transform.position
                    );
            }
            else
            {
                m0 =
                    controlPoints[j + 1].transform.position -
                    controlPoints[j].transform.position;
            }
            if (j < controlPoints.Count - 2)
            {
                m1 =
                    0.5f *
                    (
                    controlPoints[j + 2].transform.position -
                    controlPoints[j].transform.position
                    );
            }
            else
            {
                m1 =
                    controlPoints[j + 1].transform.position -
                    controlPoints[j].transform.position;
            }

            Vector3 position;
            float t;
            float pointStep = 1.0f / numberOfPoints;

            if (j == controlPoints.Count - 2)
            {
                pointStep = 1.0f / (numberOfPoints - 1.0f);
            }
            for (int i = 0; i < numberOfPoints; i++)
            {
                t = i * pointStep;
                position =
                    (2.0f * t * t * t - 3.0f * t * t + 1.0f) * p0 +
                    (t * t * t - 2.0f * t * t + t) * m0 +
                    (-2.0f * t * t * t + 3.0f * t * t) * p1 +
                    (t * t * t - t * t) * m1;

                linePoints[i + j * numberOfPoints] = position;
                //this.lineRender.SetPosition(i + j * numberOfPoints,
                //	position);
            }
        }
    }

    private IEnumerator AnimateLine()
    {
        int n = this.lineRender.positionCount;
        float segmentDuration = animationDuration / n;

        for (int i = 0; i < n - 1; i++)
        {
            float startTime = Time.time;

            Vector3 startPosition = linePoints[i];
            Vector3 endPosition = linePoints[i + 1];

            Vector3 pos = startPosition;
            if(i == 0){
                this.lineRender.SetPosition(0, pos-gameObject.transform.position);
            }
            float t = 0.0f;

            while (t < 1.0f)
            {
                t = (Time.time - startTime) / segmentDuration;
                pos = Vector3.Lerp(startPosition, endPosition, t);

                for (int j = i+1; j < n; j++)
                {
                    this.lineRender.SetPosition(j, pos-gameObject.transform.position);
                }

                yield return null;
            }
        }
    }
}
