using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SliderController : MonoBehaviour
{
    
    [Header("On Change")]
    public UnityEvent onChange;
    public StepSlider stepSlider;
    public float value = 0.0f;


    void Update()
    {
        value = stepSlider.value;
    }

    public void SetValue(float v)
    {
        stepSlider.SetValue(v);
        value = v;
    }

    public void OnChangeSlider()
    {
        onChange.Invoke();
    }

}
