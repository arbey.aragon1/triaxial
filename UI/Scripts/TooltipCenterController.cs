using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipCenterController : MonoBehaviour
{
    public List<Transform> centersTarget;
    public List<TooltipLabelController> tooltipLabelControllerList;
    public bool updateOnlyOnce = true;

    private void Start()
    {
        StartCoroutine(UpdateBound());
    }

    public Vector3 GetMeanDirection(Vector3 trm)
    {
        Vector3 dir = new Vector3(0,0,0);
        float distance;
        foreach (Transform item in this.centersTarget)
        {
            distance = Vector3.Distance(item.position, trm) + 0.1f;
            dir -= (1.0f / distance) * (item.position - trm).normalized;
        }
        return dir.normalized;
    }

    IEnumerator UpdateBound()
    {
        if(updateOnlyOnce){
            yield return new WaitForSeconds(2);
            for (int i = 0; i < tooltipLabelControllerList.Count; i++)
            {
                tooltipLabelControllerList[i].UpdateReferences();
            }
        } else {
            while(true){
                yield return new WaitForSeconds(5);
                for (int i = 0; i < tooltipLabelControllerList.Count; i++)
                {
                    tooltipLabelControllerList[i].UpdateReferences();
                }
            }
        }
        
    }
}
