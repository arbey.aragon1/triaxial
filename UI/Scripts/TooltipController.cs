using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipController : MonoBehaviour
{
    public TMPro.TextMeshProUGUI text;

    private GameObject _camera;
    private const float _distance = 1.5f;
    public List<GameObject> bounds;

    void Start () {
        _camera = GameObject.Find ("Main Camera");
        
    }
    void Update () {
        HeadLock (gameObject, 5.0f);
    }

    public void HeadLock (GameObject obj, float speed) {
        speed = Time.deltaTime * speed;
        Quaternion rotTo = Quaternion.LookRotation (-_camera.transform.position + obj.transform.position);
        obj.transform.rotation = Quaternion.Slerp (obj.transform.rotation, rotTo, speed);
        //obj.transform.position = _camera.transform.position + _camera.transform.forward * 2.0f;
    }


    public void SetText(string txt)
    {
        this.text.text = txt;
    }

    public void SetActive(bool active)
    {
        this.gameObject.SetActive(active);
    }

    

}
