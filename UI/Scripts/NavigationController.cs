using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationController : MonoBehaviour
{
    public enum Paths {
        mainMenukey, 
        triaxialComponentKey,
        targetsComponentKey,
        equipmentComponentKey,
        testComponentKey,
        UU_CD_CU_Key,
        quizComponentKey,
        pipelineComponentKey
        };
    public Controller mainMenu;
    public Controller triaxialComponent;
    public Controller targetsComponent;
    public Controller equipmentComponent;
    public Controller testComponent;
    public Controller UU_CD_CU_Component;
    public Controller quizComponent;
    public Controller pipelineComponent;

    public Controller footer;
    public Controller header;


    public Dictionary<Paths, Controller> components; 

    public enum BufferKeys {
        quizKey,
        stepKey,

    }
    private Dictionary<BufferKeys, System.Object> buffer =  new Dictionary<BufferKeys, System.Object>();
    public Dictionary<BufferKeys, System.Object>  Buffer { 
        get {return buffer; } 
        private set {buffer = value; } 
    }

    private Stack<Paths> history = new Stack<Paths>();
    
    void Start()
    {
        components = new Dictionary<Paths, Controller>(){
            {NavigationController.Paths.mainMenukey, mainMenu},
            {NavigationController.Paths.triaxialComponentKey, triaxialComponent},
            {NavigationController.Paths.targetsComponentKey, targetsComponent},
            {NavigationController.Paths.equipmentComponentKey, equipmentComponent},
            {NavigationController.Paths.testComponentKey, testComponent},
            {NavigationController.Paths.UU_CD_CU_Key, UU_CD_CU_Component},
            {NavigationController.Paths.quizComponentKey, quizComponent},
            {NavigationController.Paths.pipelineComponentKey, pipelineComponent}
        };

        history.Push(NavigationController.Paths.mainMenukey);
        this.Navigate(NavigationController.Paths.mainMenukey);
    }

    public void AddBuffer(BufferKeys key, System.Object val ){
        if (!buffer.ContainsKey(key)){
            buffer.Add(key, val);
        } else {
            buffer[key] = val;
        }
    }

    public void Navigate(NavigationController.Paths path)
    {
        if(components.ContainsKey(path))
        {
            MainPath(path);
            Paths lastVisited = history.Pop();
            components[lastVisited].SetActive(false);
            history.Push(lastVisited);
            components[path].SetActive(true);
            components[path].Constructor();
            history.Push(path);
        }
    }

    public void MainPath(Paths path)
    {
        if(
            (path == NavigationController.Paths.mainMenukey) ||
            (path == NavigationController.Paths.quizComponentKey) ||
            (path == NavigationController.Paths.pipelineComponentKey)
            )
        {
            footer.SetActive(false);
            header.SetActive(false);
        } 
        else
        {
            footer.SetActive(true);
            header.SetActive(true);
        }
    }

    public void Back()
    {
        Paths lastVisited = history.Pop();
        components[lastVisited].SetActive(false);
        lastVisited = history.Pop();
        components[lastVisited].SetActive(true);
        components[lastVisited].Constructor();
        history.Push(lastVisited);
        MainPath(lastVisited);
    }
}
