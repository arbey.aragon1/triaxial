using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FooterController : Controller
{
    public NavigationController navigationController;
    public MenuFollowController menuFollowController;
    
    public override void Constructor(){

    }

    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    public void OnBtn1()
    {
        navigationController.Back();
    }
    
    public void OnBtn2()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.quizKey,UU_CU_CD_Controller.Keys.UUKey);
        navigationController.Navigate(NavigationController.Paths.UU_CD_CU_Key);
    }

    public void OnBtn3()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.quizKey,UU_CU_CD_Controller.Keys.CUKey);
        navigationController.Navigate(NavigationController.Paths.UU_CD_CU_Key);
    }

    public void OnBtn4()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.quizKey,UU_CU_CD_Controller.Keys.CDKey);
        navigationController.Navigate(NavigationController.Paths.UU_CD_CU_Key);
    }

    public void OnBtn5()
    {
        menuFollowController.UpdateFollow();
    }
}
