using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : Controller
{
    public NavigationController navigationController;
    
    public override void Constructor(){

    }

    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    public void OnBtn1()
    {   
        navigationController.Navigate(NavigationController.Paths.triaxialComponentKey);
        
    }
    
    public void OnBtn2()
    {
        
    }

    public void OnBtn3()
    {
        
    }

}
