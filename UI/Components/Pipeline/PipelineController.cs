using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipelineController : Controller
{
    public TMPro.TextMeshProUGUI title;
    public enum PipelineKeys { 
        step1 = 0, 
        step2 = 1, 
        step3 = 2, 
        step4 = 3, 
        step5 = 4, 
        step6 = 5 
    };
    private PipelineKeys key;
    public Dictionary<PipelineKeys, string> labels = new Dictionary<PipelineKeys, string>(){
        {PipelineKeys.step1, "Preparación y montaje de la muestra y del equipo triaxial"},
        {PipelineKeys.step2, "Proceso de saturación"},
        {PipelineKeys.step3, "Proceso de consolidación"},
        {PipelineKeys.step4, "Proceso de corte"},
        {PipelineKeys.step5, "Desmonte de cámara y muestra"},
        {PipelineKeys.step6, "Resultados"}
    };
    public NavigationController navigationController;
    
    public override void Constructor(){
        this.key = (PipelineKeys)navigationController.Buffer[NavigationController.BufferKeys.stepKey];
        title.text = labels[key];
    }


    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    public void OnBtn1()
    {
        navigationController.Navigate(NavigationController.Paths.triaxialComponentKey);
    }
    
    public void OnBtn2()
    {
        int index = (int)this.key;
        if(index < 5){
            index++;
            PipelineKeys nextKey = (PipelineKeys)index;
            navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, nextKey);
            navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
        } else {
            navigationController.Navigate(NavigationController.Paths.triaxialComponentKey);
        }
        
    }
}
