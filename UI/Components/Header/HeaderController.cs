using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeaderController : Controller
{
    public NavigationController navigationController;
    public SceneManagerController scm;    
    public override void Constructor(){

    }

    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    public void OnBtn1()
    {
        navigationController.Navigate(NavigationController.Paths.triaxialComponentKey);
    }
    
    public void OnBtn2()
    {
        navigationController.Navigate(NavigationController.Paths.targetsComponentKey);
    }

    public void OnBtn3()
    {
        navigationController.Navigate(NavigationController.Paths.equipmentComponentKey);
        
        scm.LoadScene(SceneManagerController.Scenes.equipmentKey);
    }

    public void OnBtn4()
    {
        navigationController.Navigate(NavigationController.Paths.testComponentKey);
    }

}
