using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller: MonoBehaviour
{ 
    public abstract void SetActive(bool v);
    public abstract void Constructor();
}

public class UU_CU_CD_Controller : Controller
{
    public enum Keys { UUKey, CDKey, CUKey};
    public NavigationController navigationController;
    public TMPro.TextMeshProUGUI title;
    public SceneManagerController scm;
    
    public override void Constructor(){
        Debug.Log("-----------");
        Debug.Log(navigationController.Buffer[NavigationController.BufferKeys.quizKey]);
        if((Keys)navigationController.Buffer[NavigationController.BufferKeys.quizKey] == Keys.CDKey){
            Debug.Log("-----------1");
            title.text = "Ensayo consolidado drenado";
        } else if((Keys)navigationController.Buffer[NavigationController.BufferKeys.quizKey] == Keys.UUKey){
            title.text = "Ensayo no consolidado no drenado";
            Debug.Log("-----------2");
        } else if((Keys)navigationController.Buffer[NavigationController.BufferKeys.quizKey] == Keys.CUKey){
            title.text = "Ensayo consolidado no drenado";
            Debug.Log("-----------3");
        }
    }

    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    public void SetTitle(string txt)
    {
        this.title.text = txt;
    }

    public void OnBtn1()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step1);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }
    
    public void OnBtn2()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step2);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }

    public void OnBtn3()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step3);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }
    
    public void OnBtn4()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step4);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }

    public void OnBtn5()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step5);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }
    
    public void OnBtn6()
    {
        navigationController.AddBuffer(NavigationController.BufferKeys.stepKey, PipelineController.PipelineKeys.step6);
        navigationController.Navigate(NavigationController.Paths.pipelineComponentKey);
    }
}
