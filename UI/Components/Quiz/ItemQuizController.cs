using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemQuizController : MonoBehaviour
{
    public QuizComponent quizComponent;
    public GameObject imgActive;
    public GameObject imgUnactive;

    public Material green;
    public Material red;
    public Material normalMat;

    public MeshRenderer meshRenderer;

    public TMPro.TextMeshProUGUI text;
    private bool status = false;

    private QuizComponent.BtnKey index;
    public QuizComponent.BtnKey Index { 
        set{ this.index = value; } 
    }

    public void SetActive(bool v)
    {
        gameObject.SetActive(v);
    }

    public void SetText(string txt)
    {
        this.text.text = txt;
    }

    public void SetStatus(bool status)
    {
        this.status = status;
    }

    public bool GetStatus()
    {
        return this.status;
    }

    public void SetColorGreen()
    {
        meshRenderer.material = green;
    }

    public void SetColorRed()
    {
        meshRenderer.material = red;
    }

    public void SetColorReset()
    {
        meshRenderer.material = normalMat;
    }

    public void ActiveStatus()
    {
        Debug.Log(this.index);
        quizComponent.SelectItem(this.index);
    }

    public void UpdateIcon()
    {
        if(this.status){
            imgActive.SetActive(true);
            imgUnactive.SetActive(false);
        }else{
            imgActive.SetActive(false);
            imgUnactive.SetActive(true);
        }
    }
}
