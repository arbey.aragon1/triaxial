using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommentController : MonoBehaviour
{
    public QuizComponent quizComponent;

    public TMPro.TextMeshProUGUI title;
    public TMPro.TextMeshProUGUI text;


    public void SetTitle(string txt)
    {
        this.title.text = txt;
    }

    public void SetText(string txt)
    {
        this.text.text = txt;
    }

    public void SetActive(bool active)
    {
        this.gameObject.SetActive(active);
    }

}