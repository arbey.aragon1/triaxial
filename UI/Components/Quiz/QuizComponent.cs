using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Quiz
{
    public string Title { get; set; }
    public string Description { get; set; }
    public int Answer { get; set; }
    public string Comment { get; set; }

    public class Item
    {
        public string Text { get; set; }
        public bool Answer { get; set; }
        public Item(string text, bool answer){
            this.Text = text;
            this.Answer = answer;
        }
    }

    public List<Item> Items { get; set; }

    public Quiz(
        string title, 
        string description, 
        string comment, 
        string item0, 
        string item1, 
        string item2, 
        int answer)
    {
        // Adicionar aleatorio
        this.Title = title;
        this.Description = description;
        this.Comment = comment;
        List<int> numbers = (new List<int>(){0,1,2}).OrderBy(w => Random.Range(0, 10)).ToList();
        List<Item> tem = new List<Item>(){
            (new Item(item0,answer == 0)),
            (new Item(item1,answer == 1)),
            (new Item(item2,answer == 2))
        };
        Items = new List<Item>();
        int counter = 0;
        foreach(int i in numbers){
            Items.Add(tem[i]);
            if(tem[i].Answer)
            {
                this.Answer = counter;
            }
            counter++;
        }
    }

    public bool IsAnswerRight(int answer)
    {
        return (answer == this.Answer);
    }
}

public class QuizComponent : Controller
{
    public TMPro.TextMeshProUGUI title;
    public TMPro.TextMeshProUGUI description;
    public ItemQuizController btn0;
    public ItemQuizController btn1;
    public ItemQuizController btn2;

    public GameObject button;

    public CommentController cmm0;
    public GameObject win;

    public enum BtnKey: int {
        btn0Key = 0,  
        btn1Key = 1,  
        btn2Key = 2  
    };
    private BtnKey currentBtnKey = BtnKey.btn0Key;
    private Dictionary<BtnKey, ItemQuizController> buttons;

    public enum QuizKey {
        q0, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15,
    };

    private QuizKey currentQuizKey = QuizKey.q0;
    private Dictionary<QuizKey, Quiz> quices;

    private bool sendedData = false;

    public NavigationController navigationController;
    
    public override void Constructor(){

    }

    public override void SetActive(bool v){
        this.gameObject.SetActive(v);
    }

    void Start()
    {
        btn0.Index = BtnKey.btn0Key;
        btn1.Index = BtnKey.btn1Key;
        btn2.Index = BtnKey.btn2Key;

        buttons = new Dictionary<BtnKey, ItemQuizController>();
        buttons.Add(BtnKey.btn0Key, btn0);
        buttons.Add(BtnKey.btn1Key, btn1);
        buttons.Add(BtnKey.btn2Key, btn2);

        quices = new Dictionary<QuizKey, Quiz>();
        quices.Add(QuizKey.q0, new Quiz(
            "Preparación y Montaje de la muestra y del equipo triaxial", 
            "¿Cuál es el diámetro mínimo que deben tener los cilindros de la muestra?", 
            "La respuesta seleccionada es incorrecta, el diámetro mínimo que deben tener los cilindros de la muestra es de 33 mm, pues muestras de menor diámetro presentarían dificultades en el tallado, montaje y ejecución del ensayo.",
            "33 mm", 
            "30 mm", 
            "40 mm",
            0));
        quices.Add(QuizKey.q1, new Quiz(
            "Proceso de saturación", 
            "¿Cuál es el objetivo de la saturación?", 
            "La respuesta seleccionada es incorrecta, el objetivo de la saturación es llenar los vacíos de la muestra con agua para lograr aplicar el principio de esfuerzos efectivos en suelos saturados.",
            "Aumentar el volumen de la muestra", 
            "Facilitar los procesos de consolidación", 
            "Llenar todos los vacíos de la muestra con agua",
            2));
        quices.Add(QuizKey.q2, new Quiz(
            "Proceso de consolidación", 
            "El tiempo de consolidación de la muestra es sensible a:", 
            "La respuesta seleccionada es incorrecta, el tiempo de consolidación de la muestra es sensible al tipo de suelo, el incremento de esfuerzos aplicados y relación de vacíos pues el tiempo de disipación del exceso de presión de poros depende de estas variables.",
            "Contenido de agua inicial e incremento de esfuerzos aplicados", 
            "La presión de poros y el tipo de suelo", 
            "La ‘permeabilidad del suelo, el incremento de esfuerzos aplicados y relación de vacíos inicial",
            2));
        quices.Add(QuizKey.q3, new Quiz(
            "Proceso de corte", 
            "Durante el proceso de corte, el exceso en la presión de poros debe ser:", 
            "La respuesta seleccionada es incorrecta, durante el proceso de corte el exceso en la presión de poros debe ser cero, pues en un ensayo drenado la válvula de drenaje está abierta lo cual no permite que se presente un exceso de presión de poros permitiendo el cambio de volumen de la muestra por el drenaje del agua; es de aclarar que la velocidad de falla de la muestra debe ser tan lenta para que esto sea posible.",
            "Cero", 
            "Igual a la presión de cámara.", 
            "Ninguna de las anteriores",
            0));
        quices.Add(QuizKey.q4, new Quiz(
            "Desmonte de cámara y muestra", 
            "Una vez se remueva la carga axial y las presiones de cámara y de contrapresión. La muestra se retira rápidamente para:",
            "La respuesta seleccionada es incorrecta, la muestra se retira rápidamente para que no haya tiempo de absorción de agua y así no varíe el contenido de agua y el peso unitario final.", 
            "Que no se consolide más.", 
            "Que no haya tiempo para que absorba agua de los discos poros", 
            "Que no se dañe la membrana.",
            1));
        quices.Add(QuizKey.q5, new Quiz(
            "Preparación y Montaje de la muestra y del equipo triaxial", 
            "La relación altura/diámetro de la muestra a fallar está entre:", 
            "La respuesta seleccionada es incorrecta, la respuesta seleccionada es incorrecta, de acuerdo con el procedimiento estándar para la realización de ensayos triaxiales, la relación de altura/diámetro debe estar entre 2.0 y 2.5.",
            "1.5 y 2", 
            "2 y 2.5", 
            "2.5 y 3",
            1));
        quices.Add(QuizKey.q6, new Quiz(
            "Proceso de saturación", 
            "Para considerar el suelo saturado, el parámetro B de Skempton tiene que ser mayor o igual", 
            "La respuesta seleccionada es incorrecta, teóricamente un suelo totalmente saturado posee un parámetro B de Skempton de 1. En suelos finos, el proceso de saturación total puede ser muy difícil de alcanzar, por tal motivo se ha aceptado en la práctica que un suelo se considera saturado si el parámetro B es mayor o igual a 0.95.",
            "0.90", 
            "0.95", 
            "1.00",
            1));
        quices.Add(QuizKey.q7, new Quiz(
            "Proceso de consolidación", 
            "Hasta qué momento como mínimo se debe mantener la carga axial en la etapa de consolidación:", 
            "La respuesta seleccionada es incorrecta, como mínimo se debe mantener la carga axial en la etapa de consolidación hasta haber completado la consolidación primaria. Lo anterior se puede verificar con los métodos de Taylor y Casagrande.",
            "Hasta completar la consolidación secundaria.", 
            "Hasta completar la consolidación primaria.", 
            "Hasta que la muestra falle.",
            1));
        quices.Add(QuizKey.q8, new Quiz(
            "Proceso de corte", 
            "Durante la etapa de corte no se debe permitir:", 
            "La respuesta seleccionada es incorrecta, durante la ejecución de un ensayo triaxial CU, el proceso de corte es no drenado. Por tal motivo, durante esta etapa las válvulas de drenaje se encuentran cerradas y no se permite el drenaje en la muestra.",
            "Saturación de la muestra", 
            "Carga axial", 
            "Drenaje de la muestra",
            2));
        quices.Add(QuizKey.q9, new Quiz(
            "Desmonte de cámara y muestra", 
            "Una vez terminada la etapa de corte lo primero que se debe hacer es:", 
            "La respuesta seleccionada es incorrecta, una vez finalizado el proceso de corte en la muestra, es necesario remover la carga axial aplicada y reducir las presiones de cámara y de contrapresión a cero. Lo anterior con el objetivo de tomar fotos que evidencien el modo de falla de la muestra y realizar ensayos de contenido de agua, límites de Atterberg y granulometría.",
            "Remover la carga axial y reducir las presiones de cámara y de contrapresión a cero", 
            "Aumentar la carga axial", 
            "Retirar la muestra de la cámara triaxial",
            1));
        quices.Add(QuizKey.q10, new Quiz(
            "Preparación y Montaje de la muestra y del equipo triaxial", 
            "El tamaño de la partícula más grande debe ser menor a:", 
            "La respuesta seleccionada es incorrecta, hay que tener en cuenta que para evitar sobretamaños durante la realización del ensayo, el tamaño de la partícula más grande debe ser menor a ⅙ del diámetro de la muestra.",
            "⅙ del diámetro de la muestra", 
            "½ de la altura de la muestra", 
            "2 veces el diámetro de la muestra",
            0));
        quices.Add(QuizKey.q11, new Quiz(
            "Proceso de saturación", 
            "El uso de agua desaireada en el proceso de saturación de la muestra sirve para:", 
            "La respuesta seleccionada es incorrecta, el agua desaireada se emplea con el objetivo de asegurar la ausencia de burbujas en ella, y por tanto de disminuir el tiempo de saturación como de disminuir la contrapresión requerida para lograr la saturación",
            "Disminuir el tiempo de saturación", 
            "Disminuir la contrapresión requerida para la saturación", 
            "Todas las anteriores",
            2));
        quices.Add(QuizKey.q12, new Quiz(
            "Proceso de consolidación", 
            "¿Para qué se coloca el papel filtro en la muestra?", 
            "La respuesta seleccionada es incorrecta, el papel filtro se emplea para proteger las piedras porosas de las partículas de suelo fino y evitar que se colmaten y pierdan las propiedades drenantes.",
            "Para impedir el paso de partículas de suelo fino hacia la piedra porosa.", 
            "Para impermeabilizar la muestra en ambos extremos.", 
            "Para impedir que la muestra se desacomode.",
            2));
        quices.Add(QuizKey.q13, new Quiz(
            "Proceso de corte", 
            "Durante el proceso de corte la presión en la cámara debe ser:", 
            "La respuesta seleccionada es incorrecta, la presión de cámara en el equipo triaxial debe ser constante durante el proceso de corte.",
            "Constante.", 
            "Variable según criterios de falla.", 
            "Variable en función de la presión de poros.",
            0));
        quices.Add(QuizKey.q14, new Quiz(
            "Desmonte de cámara y muestra.", 
            "Luego de terminar el ensayo triaxial, se debe determinar en la muestra:", 
            "La respuesta seleccionada es incorrecta, luego de culminar el ensayo es necesario desmontar la muestra y determinar su contenido de agua.",
            "Su volumen final.", 
            "El contenido de agua final. ", 
            "La relación altura/Diámetro final.",
            1));
        SetQuiz(QuizKey.q0);
        
        //StartCoroutine(StartCor());
    }

    public void SelectItem(QuizComponent.BtnKey index)
    {
        if(!sendedData){
            foreach(var k in buttons.Keys)
            {
                buttons[k].SetStatus(false);
                buttons[k].UpdateIcon();
            }
            buttons[index].SetStatus(true);
            buttons[index].UpdateIcon();
            this.currentBtnKey = index;
        }
    }

    public void SetQuiz(QuizKey key)
    {
        this.currentQuizKey = key;
        title.text = quices[key].Title;
        description.text = quices[key].Description;
        btn0.SetText(quices[key].Items[0].Text);
        btn1.SetText(quices[key].Items[1].Text);
        btn2.SetText(quices[key].Items[2].Text);

        foreach(var k in buttons.Keys)
        {
            buttons[k].SetStatus(false);
            buttons[k].UpdateIcon();
        }
        cmm0.SetTitle("Retroalimentación:");
        cmm0.SetText(quices[key].Comment);
        cmm0.SetActive(false);
        win.SetActive(false);

        btn0.SetActive(true);
        btn1.SetActive(true);
        btn2.SetActive(true);

        btn0.SetColorReset();
        btn1.SetColorReset();
        btn2.SetColorReset();

        button.SetActive(true);
        sendedData = false;
    }

    public void SendData()
    {
        if(quices[this.currentQuizKey].IsAnswerRight((int) this.currentBtnKey))
        {
            buttons[currentBtnKey].SetColorGreen();
            win.SetActive(true);
            btn0.SetActive(false);
            btn1.SetActive(false);
            btn2.SetActive(false);

        } else {
            buttons[currentBtnKey].SetColorRed();
            cmm0.SetActive(true);
        }
        button.SetActive(false);
        sendedData = true;
        
        StartCoroutine(StartCor());
    }

    IEnumerator StartCor()
    {
        yield return new WaitForSeconds(5.0f);
        
        QuizKey key = (QuizKey) Random.Range(0, 15);
        SetQuiz(key);
    }
}
