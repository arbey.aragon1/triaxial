using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerController : MonoBehaviour
{
    public enum Scenes {
        menuKey,
        preparationAndAssemblyKey,
        equipmentKey,
    } 
    public void LoadScene(Scenes sc){
        if(sc == Scenes.menuKey)
            SceneManager.LoadScene("Menu");
        else if(sc == Scenes.preparationAndAssemblyKey)
            SceneManager.LoadScene("PreparationAndAssembly");
        else if(sc == Scenes.equipmentKey)
            SceneManager.LoadScene("Equipment");
    }
}
