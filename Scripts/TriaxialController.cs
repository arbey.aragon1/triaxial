using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriaxialController : MonoBehaviour
{
    public MultiLineGenerator v1;
    public MultiLineGenerator v2;
    public MultiLineGenerator v3;
    public MultiLineGenerator v4;
    public MultiLineGenerator v5;

    private void Start()
    {
        //v1.Fill();
    }

    private void EmptyAll()
    {
        v1.Empty();
        v2.Empty();
        v3.Empty();
        v4.Empty();
        v5.Empty();
    }

    public void Valve1()
    {
        v1.RunLine(5, 2.5f);
    }

    public void Valve2()
    {
        v2.RunLine(5, 3.0f);
    }

    public void Valve3()
    {
        v3.RunLine(5, 2.5f);
    }

    public void Valve4()
    {
        v4.RunReverseLine(5, 7.0f);
    }

    public void Valve5()
    {
        v5.RunLine(5, 2.5f);
    }
    
}
