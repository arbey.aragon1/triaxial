using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSingleton : MonoBehaviour
{
    public static LightSingleton instance;
    void Awake()
    {
        if(LightSingleton.instance == null) {
            LightSingleton.instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    
}
