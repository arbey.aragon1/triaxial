using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayspaceSingleton : MonoBehaviour
{
    public static PlayspaceSingleton instance;
    void Awake()
    {
        if(PlayspaceSingleton.instance == null) {
            PlayspaceSingleton.instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    
}
