using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolkitSingleton : MonoBehaviour
{
    public static ToolkitSingleton instance;
    void Awake()
    {
        if(ToolkitSingleton.instance == null) {
            ToolkitSingleton.instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    
}
